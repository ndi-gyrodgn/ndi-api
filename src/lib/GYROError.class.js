module.exports = class GYROError extends Error {

	constructor(status, message) {

		super(message);

		this.name = this.constructor.name;

		Error.captureStackTrace(this, this.constructor);

		// `500` is the default value if not specified.
		this.status = status || 500;
	}

	static fromError(e) {
		return new GYROError(e.errno, e.message);
	}

};

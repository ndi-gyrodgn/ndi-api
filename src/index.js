const Koa = require('koa');
const app = new Koa();
const config = require('../config/index');

const errorMdl = require('./middlewares/error');

const bodyParser = require('koa-bodyparser');

const router = require('./routes/index');

app.use(bodyParser());
app.use(errorMdl.handleError);

app.use(async(c, n) => {

	c.response.type = 'application/json';
	c.state = {};

	await n();
});

app.use(router.routes());

const server = app.listen(config.port);


server.on('listening', async () => {

	console.log('GYRO-API running on port', config.port);

	server.emit('ready');
});

server.on('close', async () => {
	console.log('GYRO-API close');
});


module.exports = server;

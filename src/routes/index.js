const router = require('koa-router')();

const helloCtrl = require('../controllers/Hello');

router.get('/hello', helloCtrl.hi);

module.exports = router;

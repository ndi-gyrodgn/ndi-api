const config = require('../../config/index');
const GYROError = require('../lib/GYROError.class');

module.exports = {
	handleError: async function (c, n) {

		try {

			await n();

		} catch (err) {

			if (err.constructor.name !== 'JdError') {

				err = GYROError.fromError(err);
			}

			c.status = err.status;

			c.body = {
				status: err.status,
				message: err.message
			};

			if (config.env === 'test') {
				console.log(err);
			}
		}
	}
};

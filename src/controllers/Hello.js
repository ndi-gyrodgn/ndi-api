
const helloCtrl = {
	async hi(c, n) {

		c.body = 'Hi !';

		await n();
	}
};

module.exports = helloCtrl;

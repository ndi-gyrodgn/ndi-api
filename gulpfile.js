const gulp = require('gulp');

const eslint = require('gulp-eslint');
const nodemon = require('gulp-nodemon');

const pump = require('pump');

const jsFiles = ['./src/*.js', './src/*/*.js'];
const dist = 'dist';

gulp.task('dev:lint', (cb) => {
	pump([
			gulp.src(jsFiles),
			eslint(),
			eslint.format(),
		],
		cb
	);
});

gulp.task('lint', (cb) => {
	pump([
			gulp.src(jsFiles),
			eslint(),
			eslint.format(),
		],
		cb
	);
});

gulp.task('dev', ['dev:lint'], () => {
	nodemon({
		script: './src/index.js',
		ext: 'js',
		tasks: ['dev:lint']
	});
});

gulp.task('prod', ['lint'], () => {
	nodemon({
		script: './bin/www',
		ext: 'js',
		env: {
			NODE_ENV: 'prod'
		}
	});
});

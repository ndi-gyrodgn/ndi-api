module.exports = {
	env: process.env.NODE_ENV,
	port: process.env.API_PORT,
	db_uri: process.env.DB_URI
};

FROM node:10.0.0

WORKDIR /var/www/

ADD . /var/www/

RUN npm install

EXPOSE 80

CMD ["node", "./src/index.js"]
